package f

import (
	"crypto"
	"crypto/ed25519"
	"encoding/base64"
	"encoding/pem"
	"image/color"
	"os"
	"sort"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/dialog"
	"fyne.io/fyne/v2/widget"
	"golang.org/x/crypto/ssh"
)

// CreateFiles creates a set of directories and files with a given permission mode.
// It checks if a working directory exists, if not, it creates the necessary directories and files.
// It generates a new ed25519 key pair and writes the private and public keys to files.
// It also writes predefined hosts, gitignore, and settings data to files.
// If any file or directory operations fail, it panics.
func CreateFiles(tabNames []string) error {
	if _, err := os.Stat(Work); err != nil {
		for _, folder := range tabNames {
			if err = os.Mkdir(Work+folder, perm); err != nil {
				return err
			}
		}
		pub, priv, err := ed25519.GenerateKey(nil)
		if err != nil {
			return err
		}
		privBlock, err := ssh.MarshalPrivateKey(crypto.PrivateKey(priv), "")
		if err != nil {
			return err
		}
		privKey := pem.EncodeToMemory(privBlock)
		pubBlock, err := ssh.NewPublicKey(pub)
		if err != nil {
			return err
		}
		pubKey := []byte("ssh-ed25519 " + base64.StdEncoding.EncodeToString(pubBlock.Marshal()))
		files := map[string][]byte{
			Work + Name:          privKey,
			Work + Name + ".pub": pubKey,
			Work + ".gitignore":  gitignore,
			Work + "config.toml": settings,
		}
		for key, val := range files {
			if err := os.WriteFile(key, val, perm); err != nil {
				return err
			}
		}
	}
	return nil
}

// LongestString returns the longest string in a slice of strings.
func LongestString(strings []string) string {
	longest := ""
	for _, str := range strings {
		if len(str) > len(longest) {
			longest = str
		}
	}
	return longest
}

// FilenamesArray returns the names of files in a specified directory.
func FilenamesArray(directory string) ([]string, error) {
	files, err := os.ReadDir(directory)
	if err != nil {
		return nil, err
	}
	var filenames []string
	for _, file := range files {
		filenames = append(filenames, file.Name())
	}
	sort.Strings(filenames)
	return filenames, nil
}

// TextSize returns the minimum size with padding of a canvas.Text object.
func TextSize(text string) fyne.Size {
	ct := canvas.NewText(text, color.White)
	ct.TextStyle.Monospace = true
	return ct.MinSize().AddWidthHeight(10, 15)
}

// Confirmation Button with optional dialog.
func ButtonConfirm(label string, tapped func(), w fyne.Window) *widget.Button {
	return widget.NewButton(label, func() {
		if Cfg.Confirm {
			dialog.ShowConfirm("Confirmation", "Run "+label+"?", func(response bool) {
				if response {
					tapped()
				}
			}, w)
		} else {
			tapped()
		}
	})
}

func PanicMessage(w fyne.Window, err error) {
	PanicMessage := canvas.NewText(err.Error(), &color.RGBA{255, 0, 0, 255})
	PanicMessage.TextStyle = fyne.TextStyle{Bold: true}
	PanicMessage.TextSize = 24
	w.SetContent(PanicMessage)
	w.ShowAndRun()
	os.Exit(1)
}
