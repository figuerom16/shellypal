package f

import (
	"errors"
	"os"
	"strings"
	"time"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/widget"
	"github.com/fyne-io/terminal"
	"golang.org/x/crypto/ssh"
	"golang.org/x/crypto/ssh/knownhosts"
)

type Shell struct {
	Connection string
	Local      *terminal.Terminal
	Remote     *terminal.Terminal
	Message    *widget.Label
	signer     ssh.Signer
	client     *ssh.Client
	session    *ssh.Session
}

// New initializes a new Shell instance.
// It creates new local and remote terminal instances and sets up a listener for terminal configuration changes.
// When a change is received, it adjusts the size of the pseudo-terminal (PTY) accordingly.
// It also sets up a welcome message and reads the private key from a file.
// If any error occurs during this process, it returns the error.
// Finally, it starts a goroutine to run the local shell in a loop.
func (s *Shell) New() error {
	s.Local = terminal.New()
	s.Remote = terminal.New()
	c := make(chan terminal.Config)
	go func() {
		rows, cols := uint(0), uint(0)
		for {
			config := <-c
			if rows == config.Rows && cols == config.Columns {
				continue
			}
			rows, cols = config.Rows, config.Columns
			s.session.WindowChange(int(rows), int(cols))
		}
	}()
	s.Remote.AddListener(c)
	s.Message = widget.NewLabel("Welcome to Shellypal!")
	s.Message.TextStyle = fyne.TextStyle{Bold: true}
	s.Message.Importance = widget.HighImportance
	privBytes, err := os.ReadFile(Work + Name)
	if err != nil {
		return err
	}
	s.signer, err = ssh.ParsePrivateKey(privBytes)
	if err != nil {
		return err
	}
	go func() {
		for {
			s.Local.RunLocalShell()
		}
	}()
	return nil
}

// RemoteShell establishes an SSH connection to a remote shell.
// It first parses the connection string to extract the user and host.
// Then, it sets up the host key callback based on the known hosts file.
// If the known hosts file is not used, it ignores the host key.
// It then dials the SSH connection and creates a new session. Requests a pseudo-terminal PTY.
// It then runs the user's default shell or bash if the default shell is not available.
// It sets up the standard input and output for the session and runs the connection.
// If any error occurs during this process, it closes the remote session and returns.
func (s *Shell) RemoteShell(tab *container.TabItem) {
	connArr := strings.Split(s.Connection, "@")
	if len(connArr) != 2 {
		s.CloseRemote(tab, errors.New("invalid connection string: format is user@host:port"))
		return
	}
	hostKeyCallback, err := knownhosts.New(Home + "/.ssh/known_hosts")
	if Cfg.Known {
		if err != nil {
			s.CloseRemote(tab, err)
			return
		}
	} else {
		hostKeyCallback = ssh.InsecureIgnoreHostKey()
	}
	user, addr := connArr[0], connArr[1]
	s.client, err = ssh.Dial("tcp", addr, &ssh.ClientConfig{
		User:            user,
		Auth:            []ssh.AuthMethod{ssh.PublicKeys(s.signer)},
		Timeout:         time.Duration(Cfg.Timeout) * time.Second,
		HostKeyCallback: hostKeyCallback,
	})
	if err != nil {
		s.CloseRemote(tab, err)
		return
	}
	s.session, err = s.client.NewSession()
	if err != nil {
		s.CloseRemote(tab, err)
		return
	}
	if err := s.session.RequestPty("xterm-256color", 24, 80, nil); err != nil {
		s.CloseRemote(tab, err)
		return
	}
	go s.session.Run("clear; $SHELL || bash")
	in, err := s.session.StdinPipe()
	if err != nil {
		s.CloseRemote(tab, err)
		return
	}
	out, err := s.session.StdoutPipe()
	if err != nil {
		s.CloseRemote(tab, err)
		return
	}
	tab.Content = s.Remote
	s.Message.Importance = widget.SuccessImportance
	s.Message.SetText("Connected to: " + s.Connection)
	if err := s.Remote.RunWithConnection(in, out); err != nil {
		s.CloseRemote(tab, err)
		return
	}
}

// CloseRemote closes the SSH session and client in the Shell struct.
// If an error is passed, it sets the error in the Shell struct and replaces the tab content with an Info widget.
// If an error occurs while closing the session or client, it panics.
func (s *Shell) CloseRemote(tab *container.TabItem, err error) {
	s.Connection = ""
	if err != nil {
		tab.Content = Info()
		s.Message.Importance = widget.DangerImportance
		s.Message.SetText("Error: " + err.Error())
	}
	if s.session != nil {
		if err := s.session.Close(); err != nil {
			s.Message.Importance = widget.DangerImportance
			s.Message.SetText("Error: " + err.Error())
		}
		s.session = nil
	}
	if s.client != nil {
		if err := s.client.Close(); err != nil {
			s.Message.Importance = widget.DangerImportance
			s.Message.SetText("Error: " + err.Error())
		}
		s.client = nil
	}
}

// WriteRead sends a command to either the local or remote terminal of the Shell struct.
// It optionally reads the output of the command and returns it as a string.
// If an error occurs during writing or reading, it is stored in the Shell struct and an empty string is returned.
func (s *Shell) WriteRead(local bool, read bool, cmd string) string {
	t := s.Remote
	if local {
		t = s.Local
	}
	if _, err := t.Write([]byte{0x3, 0xD, 'c', 'l', 'e', 'a', 'r', 0xD}); err != nil {
		s.Message.Importance = widget.DangerImportance
		s.Message.SetText("Error: " + err.Error())
		return ""
	}
	time.Sleep(100 * time.Millisecond)
	prompt := strings.TrimSpace(t.Text())
	if _, err := t.Write(append([]byte(cmd), 0xD)); err != nil {
		s.Message.Importance = widget.DangerImportance
		s.Message.SetText("Error: " + err.Error())
		return ""
	}
	if !read {
		return ""
	}
	output := []string{""}
	for range 10 * Cfg.Timeout {
		time.Sleep(100 * time.Millisecond)
		output = strings.Split(t.Text(), "\n")
		for output[len(output)-1] == "" {
			output = output[:len(output)-1]
		}
		if len(output) < 2 {
			continue
		}
		if strings.TrimSpace(output[len(output)-1]) == prompt {
			if len(output) < 3 {
				return ""
			}
			break
		}
	}
	if len(output) < 3 {
		s.Message.Importance = widget.DangerImportance
		s.Message.SetText("Error: terminal read timeout")
		return ""
	}
	return strings.Join(output[1:len(output)-1], "\n")
}

// Run executes a shell command on a remote server via SSH. It takes a connection string in the format "user@host:port" and a command string as input.
// It returns the output of the command as a string. If an error occurs during the process, it sets the error and returns an empty string.
// The function also writes the output of the command to a log file in the "Work/Multi" directory.
func (s *Shell) Run(connection string, cmd string) string {
	connArr := strings.Split(connection, "@")
	if len(connArr) != 2 {
		s.Message.Importance = widget.DangerImportance
		s.Message.SetText("Error: invalid connection string: format is user@host:port")
		return ""
	}
	hostKeyCallback, err := knownhosts.New(Home + "/.ssh/known_hosts")
	if Cfg.Known {
		if err != nil {
			s.Message.Importance = widget.DangerImportance
			s.Message.SetText("Error: " + err.Error())
			return ""
		}
	} else {
		hostKeyCallback = ssh.InsecureIgnoreHostKey()
	}
	user, addr := connArr[0], connArr[1]
	client, err := ssh.Dial("tcp", addr, &ssh.ClientConfig{
		User:            user,
		Auth:            []ssh.AuthMethod{ssh.PublicKeys(s.signer)},
		Timeout:         time.Duration(Cfg.Timeout) * time.Second,
		HostKeyCallback: hostKeyCallback,
	})
	if err != nil {
		s.Message.Importance = widget.DangerImportance
		s.Message.SetText("Error: " + err.Error())
		return ""
	}
	defer client.Close()
	session, err := client.NewSession()
	if err != nil {
		s.Message.Importance = widget.DangerImportance
		s.Message.SetText("Error: " + err.Error())
		return ""
	}
	defer session.Close()
	output, err := session.CombinedOutput(cmd)
	if err != nil {
		s.Message.Importance = widget.DangerImportance
		s.Message.SetText("Error: " + err.Error())
		return ""
	}
	return string(output)
}

// RunFile executes a shell command on a remote server via SSH. It takes a connection string in the format "user@host:port", a pre-command string, a file in byte slice format, and a channel for error handling as input.
// It establishes an SSH connection to the remote server, executes the pre-command and the command from the file, and writes the output to a log file.
// If an error occurs during the process, it sends the error to the provided channel and returns.
func (s *Shell) RunFile(connection string, cmd string, c chan error) {
	connArr := strings.Split(connection, "@")
	if len(connArr) != 2 {
		c <- errors.New("invalid connection string: format is user@host:port")
		return
	}
	hostKeyCallback, err := knownhosts.New(Home + "/.ssh/known_hosts")
	if Cfg.Known {
		if err != nil {
			c <- err
			return
		}
	} else {
		hostKeyCallback = ssh.InsecureIgnoreHostKey()
	}
	user, addr := connArr[0], connArr[1]
	client, err := ssh.Dial("tcp", addr, &ssh.ClientConfig{
		User:            user,
		Auth:            []ssh.AuthMethod{ssh.PublicKeys(s.signer)},
		Timeout:         time.Duration(Cfg.Timeout) * time.Second,
		HostKeyCallback: hostKeyCallback,
	})
	if err != nil {
		c <- err
		return
	}
	defer client.Close()
	session, err := client.NewSession()
	if err != nil {
		c <- err
		return
	}
	defer session.Close()
	output, err := session.CombinedOutput(cmd)
	if err != nil {
		c <- err
		return
	}
	if err := os.WriteFile(Work+"Multi/"+connection+".log", output, perm); err != nil {
		c <- err
		return
	}
	c <- nil
}
