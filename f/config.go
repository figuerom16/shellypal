package f

import (
	"errors"
	"os"
	"strings"

	"fyne.io/fyne/v2/widget"
	"github.com/pelletier/go-toml/v2"
)

var (
	Cfg     *config
	Name    = "Shellypal"
	Version = "v0.1.1"
	Home, _ = os.UserHomeDir()
	Work    = Home + "/" + Name + "/"
	perm    = os.FileMode(0700)

	gitignore = []byte(Name + `*
Downloads/*
Uploads/*
Multi/*`)

	settings = []byte(`# More information at: https://gitlab.com/figuerom16/shellypal
# CONFIGURATION FILE CHANGES TAKE EFFECT AFTER RESTARTING SHELLYPAL.

# SSH timeout for all interactions in seconds (cannot be 0)
# If you have a slow connection, increase this number.
timeout = 1

# Application Width and Height
width = 900
height = 500

# Confirm before running commands
confirm = true

# Use known_hosts file for security
known = true

# Terminal CMD Shortcuts. Format lable|cmd.
shortcuts = [
	"Echo Hello|echo Hello World",
	"Status|systemctl status",
	"Failes|systemctl list-units --failed",
]

[connections]
# Format user@host:port. Left menu hosts.
hosts = [
	"user@host:port", #update me!
]

# Multi host groups.
example_group = [
	"user@random_host1:22",
	"user@random_host2:22",
	"user@random_host3:22",
]`)
)

type config struct {
	Timeout     int
	Width       float32
	Height      float32
	Confirm     bool
	Known       bool
	Shortcuts   []string
	Connections map[string][]string
}

func ReadConfig() error {
	file, err := os.ReadFile(Work + "config.toml")
	if err != nil {
		return err
	}
	Cfg = new(config)
	if err := toml.Unmarshal(file, Cfg); err != nil {
		return err
	}
	if Cfg.Timeout < 1 {
		return errors.New("panic: timeout must be greater than 0")
	}
	for _, short := range Cfg.Shortcuts {
		if len(strings.SplitN(short, "|", 2)) != 2 {
			return errors.New("panic: invalid shortcut format" + short)
		}
	}
	return nil
}

func Info() *widget.Label {
	return widget.NewLabel(`
=== SSH DISCONNECTED ===
WORK FOLDER: ` + Work + `
Use Shellypal key with local terminal outside of this program for SSH.
From Work folder you must copy Shellypal.pub and Shellypal to
` + Home + `/.ssh folder.

To change host list in left column or update settings:
-Open Work folder by clicking the top left corner.
-Edit config.toml file
-Add hosts under [connections] section hosts array.
-Close and reopen Shellypal.
-All settings: Timeouts, Shortcuts, MULTI groups are configured in config.toml file.

+Left column connects to Host.
+LOCAL: This computers terminal.
+SSH: Shows terminal when connected.
+EXEC: Run Scripts in SSH tab.
+DL: Download files/folders.
+UP: Upload files/folders.
+MULTI: Run commands on multiple hosts. Log output to Multi folder.
(NOTE: MULTI does not use selected left column host.  It operates independently.)`)
}
