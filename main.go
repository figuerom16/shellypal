package main

import (
	"errors"
	"fmt"
	"os"
	"runtime"
	"sort"
	"strings"
	"time"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
	"github.com/fyne-io/terminal"
	"gitlab.com/figuerom16/shellypal/f"
)

func main() {
	// Init app and window
	a := app.New()
	w := a.NewWindow(f.Name + " " + f.Version)
	w.SetMaster()

	// Command based on OS
	openCmd, sshCopyCmd := "", ""
	switch runtime.GOOS {
	case "linux", "darwin":
		openCmd = "open " + f.Work
		sshCopyCmd = "ssh-copy-id -i " + f.Work + f.Name + ".pub -p {PORT} {HOST}"
	case "windows":
		openCmd = "start " + f.Work
		sshCopyCmd = "type " + f.Work + f.Name + ".pub" + ` | ssh {HOST} -P {PORT} "cat >> .ssh/authorized_keys"`
	default:
		f.PanicMessage(w, errors.New("panic: unsupported os"))
	}

	// Init files and config
	if err := f.CreateFiles([]string{"", "Scripts", "Downloads", "Uploads", "Multi"}); err != nil {
		f.PanicMessage(w, err)
	}
	if err := f.ReadConfig(); err != nil {
		f.PanicMessage(w, err)
	}

	// Init Window and Shell
	w.Resize(fyne.NewSize(f.Cfg.Width, f.Cfg.Height))
	s := new(f.Shell)
	if err := s.New(); err != nil {
		f.PanicMessage(w, err)
	}

	// Topleft folder for opening Shellypal Work folder
	folder := widget.NewButtonWithIcon("Work", theme.FolderIcon(), func() {
		s.WriteRead(true, false, openCmd)
	})

	// EXEC tab container
	execEntry := widget.NewEntry()
	execEntry.SetPlaceHolder("(OPTIONAL) Enter command that will run before script.")
	execTab := container.NewBorder(execEntry, nil, nil, nil, f.Info())

	// Multi tab container
	groups := make([]string, 0, len(f.Cfg.Connections))
	for k := range f.Cfg.Connections {
		groups = append(groups, k)
	}
	var group []string
	multiCombo := widget.NewSelect(groups, func(value string) {
		group = f.Cfg.Connections[value]
	})
	multiCombo.PlaceHolder = "Choose Host Group to run command on."
	multiEntry := widget.NewEntry()
	multiEntry.SetPlaceHolder("(OPTIONAL) Enter command that will run before script.")
	multiTab := container.NewBorder(container.NewVBox(multiCombo, multiEntry), nil, nil, nil, widget.NewLabel("=== No files in Scripts folder. ==="))

	// Tab containers
	tabs := container.NewAppTabs(
		container.NewTabItemWithIcon("LOCAL", theme.ComputerIcon(), s.Local),
		container.NewTabItemWithIcon("SSH", theme.ComputerIcon(), f.Info()),
		container.NewTabItemWithIcon("EXEC", theme.FileTextIcon(), execTab),
		container.NewTabItemWithIcon("DL", theme.DownloadIcon(), f.Info()),
		container.NewTabItemWithIcon("UL", theme.UploadIcon(), f.Info()),
		container.NewTabItemWithIcon("MULTI", theme.StorageIcon(), multiTab),
	)
	tabs.SetTabLocation(container.TabLocationTop)
	tabs.OnSelected = func(ti *container.TabItem) {
		if t, ok := ti.Content.(*terminal.Terminal); ok {
			tabs.Refresh()
			w.Canvas().Focus(t)
		}
		switch ti.Text {
		case "EXEC":
			if s.Connection == "" {
				execTab.Objects[0] = f.Info()
				return
			}
			items, err := f.FilenamesArray(f.Work + "Scripts")
			if err != nil {
				s.Message.Importance = widget.DangerImportance
				s.Message.SetText("Error: " + err.Error())
				return
			}
			if len(items) == 0 {
				execTab.Objects[0] = widget.NewLabel("=== No files in Scripts folder. ===")
				return
			}
			grid := container.NewGridWrap(f.TextSize(f.LongestString(items)))
			for _, item := range items {
				grid.Add(f.ButtonConfirm(item, func() {
					file, err := os.ReadFile(f.Work + "Scripts/" + item)
					if err != nil {
						s.Message.Importance = widget.DangerImportance
						s.Message.SetText("Error: " + err.Error())
						return
					}
					s.WriteRead(false, false, execEntry.Text+"\n"+string(file))
					tabs.Select(tabs.Items[1])
				}, w))
			}
			execTab.Objects[0] = container.NewVScroll(grid)
		case "DL":
			if s.Connection == "" {
				ti.Content = f.Info()
				return
			}
			connArr := strings.Split(s.Connection, ":")
			host, port := connArr[0]+":", connArr[1]+" "
			pwd := s.WriteRead(false, true, "pwd") + "/"
			items := s.Run(s.Connection, "ls -1A "+pwd)
			if items == "" {
				ti.Content = widget.NewLabel("=== No files in Remote folder. ===")
				return
			}
			itemArr := strings.Split(items, "\n")
			sort.Strings(itemArr)
			grid := container.NewGridWrap(f.TextSize(f.LongestString(itemArr)))
			for _, item := range itemArr {
				if item == "" {
					continue
				}
				grid.Add(f.ButtonConfirm(item, func() {
					s.WriteRead(true, false, fmt.Sprintf("scp -r -i %s%s -P %s%s%s%s %sDownloads/",
						f.Work, f.Name, port, host, pwd, item, f.Work))
					tabs.Select(tabs.Items[0])
				}, w))
			}
			ti.Content = container.NewVScroll(grid)
		case "UL":
			if s.Connection == "" {
				ti.Content = f.Info()
				return
			}
			connArr := strings.Split(s.Connection, ":")
			host, port := connArr[0]+":", connArr[1]+" "
			pwd := s.WriteRead(false, true, "pwd") + "/"
			items, err := f.FilenamesArray(f.Work + "Uploads")
			if err != nil {
				s.Message.Importance = widget.DangerImportance
				s.Message.SetText(" " + err.Error())
				return
			}
			if len(items) == 0 {
				ti.Content = widget.NewLabel("=== No files in Uploads folder. ===")
				return
			}
			grid := container.NewGridWrap(f.TextSize(f.LongestString(items)))
			for _, item := range items {
				grid.Add(f.ButtonConfirm(item, func() {
					s.WriteRead(true, false, fmt.Sprintf("scp -r -i %s%s -P %s %sUploads/%s %s%s",
						f.Work, f.Name, port, f.Work, item, host, pwd))
					tabs.Select(tabs.Items[0])
				}, w))
			}
			ti.Content = container.NewVScroll(grid)
		case "MULTI":
			items, err := f.FilenamesArray(f.Work + "Scripts")
			if err != nil {
				s.Message.Importance = widget.DangerImportance
				s.Message.SetText("Error: " + err.Error())
				return
			}
			if len(items) == 0 {
				multiTab.Objects[0] = widget.NewLabel("=== No files in Scripts folder. ===")
				return
			}
			grid := container.NewGridWrap(f.TextSize(f.LongestString(items)))
			for _, item := range items {
				grid.Add(f.ButtonConfirm(item, func() {
					if len(group) == 0 {
						s.Message.Importance = widget.DangerImportance
						s.Message.SetText("=== No host group selected. ===")
						return
					}
					file, err := os.ReadFile(f.Work + "Scripts/" + item)
					if err != nil {
						s.Message.Importance = widget.DangerImportance
						s.Message.SetText("ERROR: " + err.Error())
						return
					}
					s.Message.Importance = widget.WarningImportance
					s.Message.SetText("Executing: " + item)
					c := make(chan error, len(group))
					success := true
					message := ""
					for _, host := range group {
						go s.RunFile(host, multiEntry.Text+"\n"+string(file), c)
						err := <-c
						if err != nil {
							success = false
							message += host + ":" + err.Error() + " | "
						}
					}
					if success {
						s.Message.Importance = widget.SuccessImportance
						s.Message.SetText("SUCCESS: Check Multi folder for logs.")
					} else {
						s.Message.Importance = widget.DangerImportance
						s.Message.SetText("ERROR: " + message)
					}
				}, w))
			}
			multiTab.Objects[0] = container.NewVScroll(grid)
		}
	}

	// Left Host list
	list := widget.NewList(
		func() int { return len(f.Cfg.Connections["hosts"]) },
		func() fyne.CanvasObject { return widget.NewLabel(f.LongestString(f.Cfg.Connections["hosts"])) },
		func(i widget.ListItemID, o fyne.CanvasObject) {
			o.(*widget.Label).SetText(f.Cfg.Connections["hosts"][i])
		},
	)
	list.OnSelected = func(id widget.ListItemID) {
		s.CloseRemote(tabs.Items[1], nil)
		tabs.Items[1].Content = f.Info()
		s.Message.Importance = widget.WarningImportance
		s.Message.SetText("Connecting to: " + f.Cfg.Connections["hosts"][id])
		s.Connection = f.Cfg.Connections["hosts"][id]
		go s.RemoteShell(tabs.Items[1])
		for range 10 * f.Cfg.Timeout {
			time.Sleep(100 * time.Millisecond)
			if t, ok := tabs.Items[1].Content.(*terminal.Terminal); ok {
				tabs.Select(tabs.Items[1])
				tabs.Refresh()
				w.Canvas().Focus(t)
				return
			}
		}
		s.Connection = ""
		if strings.Contains(s.Message.Text, "Error: known_hosts: no such file or directory") || s.Message.Text == "Error: ssh: handshake failed: knownhosts: key is unknown" {
			tabs.Select(tabs.Items[0])
			conn := strings.Split(f.Cfg.Connections["hosts"][id], ":")
			cmd := strings.Replace(sshCopyCmd, "{HOST}", conn[0], 1)
			cmd = strings.Replace(cmd, "{PORT}", conn[1], 1)
			s.WriteRead(true, false, cmd)
			s.Message.Importance = widget.WarningImportance
			s.Message.SetText("Add Host Key to known_hosts and try again.")
		}
		list.UnselectAll()
	}

	// Bottom shortcut buttons
	shortcuts := container.NewHBox()
	for _, short := range f.Cfg.Shortcuts {
		shortArr := strings.SplitN(short, "|", 2)
		shortcuts.Add(widget.NewButton(shortArr[0], func() {
			switch tabs.Selected().Text {
			case "LOCAL":
				s.WriteRead(true, false, shortArr[1])
			case "SSH":
				if s.Connection == "" {
					s.Message.Importance = widget.DangerImportance
					s.Message.SetText("=== No host selected. ===")
					return
				}
				s.WriteRead(false, false, shortArr[1])
			default:
				s.Message.Importance = widget.DangerImportance
				s.Message.SetText("=== No command for this tab. ===")
				return
			}
			s.Message.Importance = widget.SuccessImportance
			s.Message.SetText("Executed Shortcut")
		}))
	}

	// Run Application
	w.SetContent(container.NewBorder(nil, shortcuts, container.NewBorder(folder, nil, nil, list), nil, container.NewBorder(s.Message, nil, nil, nil, tabs)))
	w.Canvas().Focus(s.Local)
	w.ShowAndRun()
}
