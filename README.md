# Shellypal
Shellypal is a GUI helper for Terminals with a focus on SSH. It is written in Go and uses the [Fyne GUI](https://github.com/fyne-io/fyne) and [Fyne Terminal](https://github.com/fyne-io/terminal) library.
The interface is designed to be minimalistic and not get in the way of the user.
All configuration is done through a single TOML file.

### AMD64
- LINUX - https://gitlab.com/figuerom16/bin/-/raw/main/linux-amd64/shellypal.tar.xz
- WINDOWS - https://gitlab.com/figuerom16/bin/-/raw/main/windows-amd64/shellypal.zip
- MACOS - https://gitlab.com/figuerom16/bin/-/raw/main/darwin-amd64/shellypal.app.tar.xz

### ARM64
- LINUX - https://gitlab.com/figuerom16/bin/-/raw/main/linux-arm64/shellypal.tar.xz
- WINDOWS - https://gitlab.com/figuerom16/bin/-/raw/main/windows-arm64/shellypal.exe.zip
- MACOS - https://gitlab.com/figuerom16/bin/-/raw/main/darwin-arm64/shellypal.app.tar.xz

## Getting Started
- Download the latest release for your OS.
- Extract the archive.
- Run Shellypal (For Linux there is a makefile for installation).
- The first time Shellypal is run it will generate the necessary files and directories in ~/Shellypal.
- Edit the config.toml file to your liking in ~/Shellypal.
- Restart Shellypal.

## Features
- [x] Auto generate ~/Shellypal working directory/files
- [x] Auto generate curve25519 keypair
- [x] Backup Shellypal by copying ~/Shellypal or creating a git repo.
- [x] Local/SSH Terminal [Fyne Terminal](https://github.com/fyne-io/terminal)
- [x] SSH Single Click connection switching.
- [x] SSH Key Fingerprinting and Verification Prompting
- [x] SSH Copy ID and Windows workaround
- [x] SSH Write script to SSH current pwd (path way directory)
- [x] SCP Download based on SSH current pwd
- [x] SCP Upload based on SSH current pwd
- [x] Execute script across multiple hosts using goroutines
- [x] Shortcut commands for common tasks

## FAQ
### Q:	I can't copy or paste?
A:	Remember to use CTRL+SHIFT+C and CTRL+SHIFT+V for Windows/Linux.
Use Command+C and Command+V for MacOS

### Q:	Why make Shellypal?
A:	I found myself using SSH, SCP, and Executing scripts everywhere. Basic maintenance, deployments, or checking on things quickly became a hassle.
Besides it wasn't too complicated since Fyne and Fyne Terminal did all the heavy lifting.

### Q:	What's special about Shellypal?
A:	Nothing, but [Fyne Terminal](https://github.com/fyne-io/terminal) is special. With Fyne Terminal the app can write/read from local/SSH terminal.
Example: In the SSH tab, navigating to a directory, then switching to the Downloads Tab will retrieve the pwd of the SSH terminal and ls -1A all files in that directory using a seperate connection.
A button grid will be generated with this information with the proper scp command to download the file. Clicking on the button will switch tab to local terminal and execute the scp command.
This is all done with three clicks. (Two if you have confirmation disabled in config.toml)

### Q:	Why is Windows Defender flagging Shellypal?
A:	Shellypal uses the [Fyne Cross Compiler](https://github.com/fyne-io/fyne-cross) for all builds. Windows Defender flags the binary as a Trojan because the binary is not signed.
Code Signing Certs cost money, but one day I might try the Windows App Store to sign the binary for free.
It also could be because the app creates a directory in the users home directory. (~/Shellypal) and utilizes the local shell.
The source code is available for anyone to review (Less than 1000 lines of code). If you don't trust the binary, build it yourself.

### Q:	I want to build this project myself?
A:	Follow the Fyne guide here: https://developer.fyne.io/started/

### Q: I want to use my own keypair instead of the auto generated one.
A:	Replace Shellypals keypair with your own. The keypair must be in the ~/Shellypal and named the same as the Shellypal keypair.

### Q:	Why is my local terminal (not Shellypal) not connecting with Shellypal generated keys?
A:	If you want your local terminal to use the Shellypal generated keys they must be manually copied from ~/Shellypal/ to ~/.ssh.
Shellypal will not do this for you; the program scope is limited to the ~/Shellypal directory.

### Q:	Why are my terminals blinding me?
A:	Fyne uses your system's theme. Set your OS theme to dark mode to save your eyes.

### Q:	Why is my SSH terminal not displaying colors?
A:	Terminal colors are based on the server you're connected to bashrc file.
Use something like this on each server to get ls colors, but your server might be differnt:
```
#Environment Settings
sed -i '/# export LS_OPTIONS/s|^# ||' ~/.bashrc
sed -i '/# eval "$(dircolors)"/s|^# ||' ~/.bashrc
sed -i "/# alias ls='ls /s|^# ||" ~/.bashrc
```

### Q:	Why is my terminal having "this" issue or missing "this" functionality?
A:	The application is built on top of Fyne Terminal. If you are having terminal specific issues, please report it to the Fyne Terminal developers.
I'm still in the process of learning the inner workings of Fyne Terminal.

## Special Thanks to the Developers of:
Fyne GUI - https://github.com/fyne-io/fyne  
Fyne Terminal - https://github.com/fyne-io/terminal  
Fyne Cross Compiler - https://github.com/fyne-io/fyne-cross  
Go-toml - https://github.com/pelletier/go-toml  

And of course the Go Standard Library.

## Screenshots
![local](https://gitlab.com/figuerom16/shellypal/-/raw/main/screenshots/local.png)
![ssh](https://gitlab.com/figuerom16/shellypal/-/raw/main/screenshots/ssh.png)
![exec](https://gitlab.com/figuerom16/shellypal/-/raw/main/screenshots/exec.png)
![download](https://gitlab.com/figuerom16/shellypal/-/raw/main/screenshots/download.png)